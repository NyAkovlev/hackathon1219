abstract class Delegate<T> {
  T get();

  Future set(T value);
}
