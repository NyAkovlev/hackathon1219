import 'package:domain/api/storage/delegate.dart';

abstract class UserStorage {
  Delegate<String> get userId;
  Delegate<String> get sessionId;
}
