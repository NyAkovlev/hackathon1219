import 'package:domain/model/network/event/event_data.dart';
import 'package:domain/model/network/session/session.dart';
import 'package:domain/model/network/card/card_data.dart';
abstract class SessionEvent {
  Stream<SessionStatus> onStatus();

  Future<Session> awaitReady();

  Future completeLoad() ;

  Stream<EventData> onEvent();

  Stream<EventData> removeEvent();

  Future pause();

  Future resume();

  Stream<CardData> addCard();

  Stream<CardData> deleteCard();
}
