import 'package:domain/model/network/room/room_data.dart';
abstract class RoomEvent {
  Stream<RoomData> onRoomData(int location);

  Stream<RoomData> onRemoveData(int location);
}
