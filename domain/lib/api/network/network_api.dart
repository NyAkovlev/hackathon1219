import 'package:domain/model/network/card/card_type.dart';
import 'package:domain/model/network/event/event_type.dart';
import 'package:domain/model/network/room/room_type.dart';
import 'package:domain/model/network/session/session.dart';
import 'package:domain/model/network/user/user.dart';
import 'package:domain/model/network/event/event.dart';
import 'package:domain/model/network/room/room.dart';
import 'package:domain/model/network/card/card.dart';

abstract class NetworkApi {
  Future<Session> getSession(String id);

  Future<List<User>> getUsers();

  Future<List<Event>> getEvents();

  Future<List<Room>> getRooms();

  Future<List<Card>> getCards();

  Future<List<EventType>> getEventTypes();

  Future<List<RoomType>> getRoomTypes();

  Future<List<CardType>> getCardTypes();
}
