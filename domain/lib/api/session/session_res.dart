import 'package:domain/api/network/network_api.dart';
import 'package:domain/api/storage/user_storage.dart';
import 'package:domain/model/network/card/card_data.dart';
import 'package:domain/model/network/card/card_type.dart';
import 'package:domain/model/network/event/event_data.dart';
import 'package:domain/model/network/event/event_type.dart';
import 'package:domain/model/network/room/room.dart';
import 'package:domain/model/network/room/room_type.dart';
import 'package:domain/model/network/user/user.dart';
import 'package:domain/model/network/event/event.dart';
import 'package:domain/model/network/card/card.dart';
import 'package:domain/model/network/session/session.dart';
import 'package:domain/model/network/user/user_data.dart';
import 'package:flutter/foundation.dart';
class SessionRes {
  final NetworkApi _bdApi;
  final UserStorage _userStorage;
  final List<User> user = [];
  final List<Event> event = [];
  final List<EventType> eventType = [];
  final List<Card> card = [];
  final List<CardType> cardType = [];
  final List<Room> room = [];
  final List<RoomType> roomType = [];
  final Session session;
  List<UserData> roomUser = [];
  int location;

  UserData get currentUser =>
      session.user.firstWhere((item) {
        return item?.id?.toString() == _userStorage.userId.get();
      });

  SessionRes(this._bdApi, this.session, this._userStorage);

  Future load() async {
    final users = Set<int>();
    final events = Set<int>();
    final eventTypes = Set<int>();
    final cards = Set<int>();
    final cardTypes = Set<int>();
    final rooms = Set<int>();
    final roomTypes = Set<int>();

    session.user.forEach((item) {
      if (item != null) {
        users.add(item.id);
        item.card?.forEach((item) {
          if (item != null) cards.add(item.id);
        });
      }
    });
    session.event.forEach((item) {
      if (item != null) events.add(item.id);
    });
    session.room.forEach((item) {
      if (item != null) rooms.add(item.id);
    });

    user.addAll(await _bdApi.getUsers(/*users*/));
    event.addAll(await _bdApi.getEvents(/*events*/));
    room.addAll(await _bdApi.getRooms(/*rooms*/));
    card.addAll(await _bdApi.getCards(/*cards*/));

    event.forEach((item) {
      if (item != null) eventTypes.add(item.type);
    });
    room.forEach((item) {
      if (item != null) roomTypes.add(item.type);
    });
    card.forEach((item) {
      if (item != null) cardTypes.add(item.type);
    });

    eventType.addAll(await _bdApi.getEventTypes(/*eventTypes*/));
    roomType.addAll(await _bdApi.getRoomTypes(/*roomTypes*/));
    cardType.addAll(await _bdApi.getCardTypes(/*cardTypes*/));
  }

  addCard(CardData card) {
    currentUser.card.add(card);
  }

  deleteCard(CardData card) {
    currentUser.card.removeWhere((item) => item?.id == card.id);
  }

  onEvent(EventData event) {
    session.event.removeWhere((item) => item?.id == event.id);
    session.event.add(event);
  }

  removeEvent(EventData event) {
    session.event.removeWhere((item) => item?.id == event.id);
  }

  onStatus(SessionStatus status) {
    session.status = describeEnum(status);
  }
}
