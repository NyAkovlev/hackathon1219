// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventType _$EventTypeFromJson(Map<String, dynamic> json) {
  return EventType()
    ..id = json['id'] as int
    ..description = json['description'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$EventTypeToJson(EventType instance) => <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'name': instance.name
    };
