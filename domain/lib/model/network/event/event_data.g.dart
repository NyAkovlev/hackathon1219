// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventData _$EventDataFromJson(Map<String, dynamic> json) {
  return EventData()
    ..id = json['id'] as int
    ..parentId = json['parentId'] as int
    ..startOnTick = json['startOnTick'] as int
    ..enabled = json['enabled'] as bool
    ..endOnTick = json['endOnTick'] as int;
}

Map<String, dynamic> _$EventDataToJson(EventData instance) => <String, dynamic>{
      'id': instance.id,
      'parentId': instance.parentId,
      'startOnTick': instance.startOnTick,
      'enabled': instance.enabled,
      'endOnTick': instance.endOnTick
    };
