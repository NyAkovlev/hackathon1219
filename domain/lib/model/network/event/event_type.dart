import 'package:json_annotation/json_annotation.dart';

part 'event_type.g.dart';

@JsonSerializable()
class EventType {
  int id;
  String description;
  String name;

  EventType();

  Map<String, dynamic> toJson() => _$EventTypeToJson(this);

  factory EventType.fromJson(Map<String, dynamic> map) =>
      _$EventTypeFromJson(map);
}
