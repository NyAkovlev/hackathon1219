import 'package:json_annotation/json_annotation.dart';

part 'event_data.g.dart';

@JsonSerializable()
class EventData {

  int id;
  int parentId;
  int startOnTick;
  bool enabled;
  int endOnTick;

  EventData();

  Map<String, dynamic> toJson() => _$EventDataToJson(this);

  factory EventData.fromJson(Map<String, dynamic> map) =>
      _$EventDataFromJson(map);
}
