import 'package:json_annotation/json_annotation.dart';

part 'room_data.g.dart';

@JsonSerializable()
class RoomData {
  int id;
  int parentId;
  int location;

  RoomData();

  Map<String, dynamic> toJson() => _$RoomDataToJson(this);

  factory RoomData.fromJson(Map<String, dynamic> map) =>
      _$RoomDataFromJson(map);
}
