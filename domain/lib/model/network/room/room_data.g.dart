// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomData _$RoomDataFromJson(Map<String, dynamic> json) {
  return RoomData()
    ..id = json['id'] as int
    ..parentId = json['parentId'] as int
    ..location = json['location'] as int;
}

Map<String, dynamic> _$RoomDataToJson(RoomData instance) => <String, dynamic>{
      'id': instance.id,
      'parentId': instance.parentId,
      'location': instance.location
    };
