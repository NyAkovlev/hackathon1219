// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomType _$RoomTypeFromJson(Map<String, dynamic> json) {
  return RoomType()
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic> _$RoomTypeToJson(RoomType instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description
    };
