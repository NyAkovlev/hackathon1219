import 'package:json_annotation/json_annotation.dart';

part 'room_type.g.dart';

@JsonSerializable()
class RoomType {
  int id;
  String name;
  String description;

  RoomType();

  Map<String, dynamic> toJson() => _$RoomTypeToJson(this);

  factory RoomType.fromJson(Map<String, dynamic> map) =>
      _$RoomTypeFromJson(map);
}
