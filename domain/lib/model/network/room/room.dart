import 'package:json_annotation/json_annotation.dart';

part 'room.g.dart';

@JsonSerializable()
class Room {
  int id;
  String name;
  int type;

  Room();

  Map<String, dynamic> toJson() => _$RoomToJson(this);

  factory Room.fromJson(Map<String, dynamic> map) =>
      _$RoomFromJson(map);
}
