import 'package:json_annotation/json_annotation.dart';

part 'card_data.g.dart';

@JsonSerializable()
class CardData {
  int id;
  int parentId;


  CardData();

  Map<String, dynamic> toJson() => _$CardDataToJson(this);

  factory CardData.fromJson(Map<String, dynamic> map) =>
      _$CardDataFromJson(map);
}
