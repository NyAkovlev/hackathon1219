import 'package:json_annotation/json_annotation.dart';

part 'card_type.g.dart';

@JsonSerializable()
class CardType {
  int id;
  String description;
  String name;

  CardType();

  Map<String, dynamic> toJson() => _$CardTypeToJson(this);

  factory CardType.fromJson(Map<String, dynamic> map) =>
      _$CardTypeFromJson(map);
}
