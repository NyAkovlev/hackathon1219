import 'package:json_annotation/json_annotation.dart';

part 'card.g.dart';

@JsonSerializable()
class Card {
  int id;
  String name;
  int type;

  Card();

  Map<String, dynamic> toJson() => _$CardToJson(this);

  factory Card.fromJson(Map<String, dynamic> map) => _$CardFromJson(map);
}
