// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CardType _$CardTypeFromJson(Map<String, dynamic> json) {
  return CardType()
    ..id = json['id'] as int
    ..description = json['description'] as String
    ..name = json['name'] as String;
}

Map<String, dynamic> _$CardTypeToJson(CardType instance) => <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'name': instance.name
    };
