import 'package:domain/model/network/card/card_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_data.g.dart';

@JsonSerializable()
class UserData {
  int id;
  double hp;
  int location;
  List<CardData> card;

  UserData();

  Map<String, dynamic> toJson() => _$UserDataToJson(this);

  factory UserData.fromJson(Map<String, dynamic> map) =>
      _$UserDataFromJson(map);
}
