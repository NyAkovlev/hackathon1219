// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserData _$UserDataFromJson(Map<String, dynamic> json) {
  return UserData()
    ..id = json['id'] as int
    ..hp = (json['hp'] as num)?.toDouble()
    ..location = json['location'] as int
    ..card = (json['card'] as List)
        ?.map((e) =>
            e == null ? null : CardData.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$UserDataToJson(UserData instance) => <String, dynamic>{
      'id': instance.id,
      'hp': instance.hp,
      'location': instance.location,
      'card': instance.card
    };
