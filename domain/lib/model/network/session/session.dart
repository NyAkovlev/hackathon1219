import 'package:domain/model/network/user/user_data.dart';
import 'package:domain/model/network/event/event_data.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:domain/model/network/room/room_data.dart';

part 'session.g.dart';

@JsonSerializable()
class Session {
  int tick;
  String status;
  List<UserData> user;
  List<EventData> event;
  List<RoomData> room;

  Session();

  Map<String, dynamic> toJson() => _$SessionToJson(this);

  factory Session.fromJson(Map<String, dynamic> map) => _$SessionFromJson(map);
}

enum SessionStatus {
  notReady,
  ready,
  loading,
  loaded,
  run,
  pause,
  finish,
}

class SessionStatusMap {
  static SessionStatus fromString(String string) {
    switch (string) {
      case "notReady":
        return SessionStatus.notReady;
      case "ready":
        return SessionStatus.ready;
      case "loading":
        return SessionStatus.loading;
      case "loaded":
        return SessionStatus.loaded;
      case "run":
        return SessionStatus.run;
      case "pause":
        return SessionStatus.pause;
      case "finish":
        return SessionStatus.finish;
      default:
        return null;
    }
  }
}