// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Session _$SessionFromJson(Map<String, dynamic> json) {
  return Session()
    ..tick = json['tick'] as int
    ..status = json['status'] as String
    ..user = (json['user'] as List)
        ?.map((e) =>
            e == null ? null : UserData.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..event = (json['event'] as List)
        ?.map((e) =>
            e == null ? null : EventData.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..room = (json['room'] as List)
        ?.map((e) =>
            e == null ? null : RoomData.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$SessionToJson(Session instance) => <String, dynamic>{
      'tick': instance.tick,
      'status': instance.status,
      'user': instance.user,
      'event': instance.event,
      'room': instance.room
    };
