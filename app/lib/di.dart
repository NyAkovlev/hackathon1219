import 'package:app/navigator/app_navigator.dart';
import 'package:async_injector/async_injector.dart';
import 'package:domain/api/storage/user_storage.dart';
import 'package:domain_impl/api/storage/user_storage_impl.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:domain/api/network/event/room_event.dart';
import 'package:domain/api/network/event/session_event.dart';
import 'package:domain_impl/api/network/event/room_event_impl.dart';
import 'package:domain_impl/api/network/event/session_event_impl.dart';
import 'package:domain/api/network/network_api.dart';
import 'package:domain_impl/api/network/bd_api_impl.dart';

class DI {
  static Provider instance;

  static T get<T>() => instance.get();

  static Future init() async {
    final builder = ProviderBuilder();
    builder.moduleImpl<SharedPreferences>(
        (inj) => SharedPreferences.getInstance());
    builder.moduleImpl<UserStorage>((inj) => UserStorageImpl(inj.get()));

    builder.moduleImpl<FirebaseApp>((inj) {
      return FirebaseApp.configure(
        name: 'hackathon1219',
        options: const FirebaseOptions(
          googleAppID: '1:855956156869:android:5182bca69b07908924dbac',
          apiKey: 'AIzaSyCO6S8MJyufL3SbgpUJYKpUWWPcLFzaeNs',
          databaseURL: 'https://hackathon1219.firebaseio.com',
        ),
      );
    });

    builder.moduleImpl<FirebaseDatabase>(
        (inj) => FirebaseDatabase(app: inj.get()));
    builder.moduleImpl<SessionEvent>(
        (inj) => SessionEventImpl(inj.get(), inj.get()));
    builder.moduleImpl<RoomEvent>((inj) => RoomEventImpl(inj.get(), inj.get()));
    builder.moduleImpl<NetworkApi>((inj) => NetworkApiImpl(inj.get()));

    builder.dependency(AppNavigator());
    instance = await builder.build();
  }
}
