import 'package:mpv/mpv.dart';
import 'package:rxdart/rxdart.dart';

mixin MainView implements View {
  final loginState = BehaviorSubject<LoginState>.seeded(LoginState.Init);

  @override
  close() {
    loginState.close();
  }

  setPrevious(String userId,String sessionId);
}

enum LoginState { Progress, InvalidSession, NameUsed, Init }
