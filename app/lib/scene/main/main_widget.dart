import 'package:app/di.dart';
import 'package:flutter/material.dart';

import 'main_presenter.dart';
import 'main_view.dart';
import 'package:flutter/widgets.dart';
import 'package:mpv/mpv.dart';

class MainWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MainWidgetState();
}

class MainWidgetState extends StateWithPresenter<MainWidget, MainPresenter>
    with MainView {
  final userIdCtrl = TextEditingController();
  final sessionIdCtrl = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  createPresenter() => MainPresenter(this, DI.get(), DI.get(), DI.get());

  @override
  setPrevious(String userId, String sessionId) {
    userIdCtrl.text = userId;
    sessionIdCtrl.text = sessionId;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Form(
              key: formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TextFormField(
                    controller: userIdCtrl,
                    decoration: InputDecoration(prefix: Text("name")),
                    validator: (value) {
                      if (value.isEmpty) {
                        return "name is empty";
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    controller: sessionIdCtrl,
                    decoration: InputDecoration(prefix: Text("session id")),
                    validator: (value) {
                      if (value.isEmpty) {
                        return "session id";
                      }
                      return null;
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            StreamWidget<LoginState>(loginState, (_, s) {
              if (s == LoginState.Progress) {
                return CircularProgressIndicator();
              }

              return Column(
                children: <Widget>[
                  if (s == LoginState.InvalidSession)
                    Text("invalid session id"),
                  if (s == LoginState.NameUsed) Text("name already used"),
                  FlatButton(
                      child: Text("enter"),
                      onPressed: () {
                        if (formKey.currentState.validate()) {
                          presenter.enterToSession(
                              userIdCtrl.text, sessionIdCtrl.text);
                        }
                      }),
                ],
              );
            })
          ],
        ),
      ),
    );
  }
}
