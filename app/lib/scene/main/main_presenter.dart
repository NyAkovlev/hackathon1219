import 'package:app/navigator/app_navigator.dart';
import 'package:domain/api/storage/user_storage.dart';
import 'package:domain/api/network/network_api.dart';
import 'main_view.dart';
import 'package:mpv/mpv.dart';

class MainPresenter extends Presenter<MainView> {
  final UserStorage _userStorage;
  final NetworkApi _bdApi;
  final AppNavigator appNavigator;

  MainPresenter(
    MainView view,
    this._userStorage,
    this._bdApi,
    this.appNavigator,
  ) : super(view) {
    view.setPrevious(_userStorage.userId.get(), _userStorage.sessionId.get());
  }

  enterToSession(String name, String sessionId) async {
    _enterToSession(name, sessionId);
  }

  _enterToSession(String userId, String sessionId) async {
    view.loginState.add(LoginState.Progress);
    try {
      final session = await _bdApi.getSession(sessionId);
      await _userStorage.userId.set(userId);
      await _userStorage.sessionId.set(sessionId);

      appNavigator.toAwaitSession();
    } catch (e) {
      view.loginState.add(LoginState.InvalidSession);
    }
  }
}
