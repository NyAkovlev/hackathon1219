import 'package:app/navigator/app_navigator.dart';
import 'package:domain/api/network/network_api.dart';
import 'package:domain/api/network/event/session_event.dart';
import 'package:domain/api/session/session_res.dart';
import 'package:domain/api/storage/user_storage.dart';
import 'await_session_view.dart';
import 'package:mpv/mpv.dart';

class AwaitSessionPresenter extends Presenter<AwaitSessionView> {
  final SessionEvent _sessionEvent;
  final UserStorage _userStorage;
  final AppNavigator _appNavigator;
  final NetworkApi _bdApi;

  AwaitSessionPresenter(
    AwaitSessionView view,
    this._sessionEvent,
    this._bdApi,
      this._appNavigator,
      this._userStorage,
  ) : super(view) {
    awaitStart();
  }

  awaitStart() async {
    try {
      view.awaitState.add(AwaitState.Await);
      final session = await _sessionEvent.awaitReady();
      view.awaitState.add(AwaitState.Loading);
      final _res = SessionRes(_bdApi, session, _userStorage);
      await _res.load();
      view.awaitState.add(AwaitState.Loaded);
      await _sessionEvent.completeLoad();
      _appNavigator.startSession(_res);
    } catch (e) {
      view.awaitState.add(AwaitState.Error);
      print(e);
    }
  }
}
