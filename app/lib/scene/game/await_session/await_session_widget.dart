import 'package:app/di.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'await_session_presenter.dart';
import 'await_session_view.dart';
import 'package:flutter/widgets.dart';
import 'package:mpv/mpv.dart';

class AwaitSessionWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AwaitSessionWidgetState();
}

class AwaitSessionWidgetState
    extends StateWithPresenter<AwaitSessionWidget, AwaitSessionPresenter>
    with AwaitSessionView {
  @override
  createPresenter() => AwaitSessionPresenter(
        this,
        DI.get(),
        DI.get(),
        DI.get(),
    DI.get(),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: StreamWidget(
          awaitState,
          (_, state) {
            return Text(state != null ? describeEnum(state) : "");
          },
        ),
      ),
    );
  }
}
