import 'package:mpv/mpv.dart';
import 'package:rxdart/rxdart.dart';

mixin AwaitSessionView implements View {
  final awaitState = BehaviorSubject.seeded(AwaitState.Await);

  @override
  close() {
    awaitState.close();
  }
}

enum AwaitState { Await, Loading, Loaded,Error }
