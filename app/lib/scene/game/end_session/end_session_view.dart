import 'package:mpv/mpv.dart';

mixin EndSessionView implements View {
  @override
  close() {}
}
