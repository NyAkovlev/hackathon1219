import 'end_session_presenter.dart';
import 'end_session_view.dart';
import 'package:flutter/widgets.dart';
import 'package:mpv/mpv.dart';

class EndSessionWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => EndSessionWidgetState();
}

class EndSessionWidgetState
    extends StateWithPresenter<EndSessionWidget, EndSessionPresenter>
    with EndSessionView {
  @override
  createPresenter() => EndSessionPresenter(this);

  @override
  Widget build(BuildContext context) {}
}
