import 'end_session_view.dart';
import 'package:mpv/mpv.dart';

class EndSessionPresenter extends Presenter<EndSessionView> {
  EndSessionPresenter(EndSessionView view) : super(view);
}
