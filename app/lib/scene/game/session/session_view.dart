import 'package:mpv/mpv.dart';
import 'package:domain/model/network/card/card_data.dart';

mixin SessionView implements View {
  Stream<CardData> cardAdd;
  Stream<CardData> cardRemove;

  @override
  close() {
  }
}
