import 'dart:ui';

import 'package:app/assets/assets.dart';
import 'package:app/di.dart';
import 'package:app/scene/game/game_card.dart';
import 'package:app/view/card/sort_stack.dart';
import 'package:app/view/circular_list.dart';
import 'package:domain/api/session/session_res.dart';
import 'package:domain/model/network/card/card_data.dart';
import 'package:flutter/material.dart';

import 'session_presenter.dart';
import 'session_view.dart';
import 'package:flutter/widgets.dart';
import 'package:mpv/mpv.dart';

class SessionWidget extends StatefulWidget {
  final SessionRes res;

  SessionWidget(this.res);

  @override
  State<StatefulWidget> createState() => SessionWidgetState();
}

class SessionWidgetState extends StateWithPresenter<SessionWidget, SessionPresenter> with SessionView {
  final List<GameCard> cardsWidget = [];
  final cardsKey = GlobalKey<SortStackState>();
  Size size;

  @override
  createPresenter() =>
      SessionPresenter(
        this,
        widget.res,
        DI.get(),
        DI.get(),
        DI.get(),
      );

  @override
  void initState() {
    super.initState();
    cardAdd.listen((card) async {
      final gameCard = GameCard(
        card,
            () {

        },
            (state, offset) {
          if (-offset.dy > cardHeight) {
            final card = (state.widget as GameCard).card;
            onCard(card);
          } else {
            cardsKey.currentState.sort();
          }
        },);
      cardsWidget.add(gameCard);
      await cardsKey.currentState.update();

      cardsKey.currentState.sort();
    });
    cardRemove.listen((card) async {
      cardsWidget.removeWhere((item) => item.card.id == card.id);
      await cardsKey.currentState.update();

      cardsKey.currentState.sort();
    });
  }

  onCard(CardData card) {

  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery
        .of(context)
        .size;

    return Scaffold(
      body: SafeArea(
        child: SortStack(
          key: cardsKey,
          cards: cardsWidget,
          size: size,
          cardSize: Size(cardHeight, cardHeight * widthScale),
          packPoint: Offset(size.height - cardHeight * 1.5, 0),
          background: Stack(
            children: <Widget>[
              SizedBox(
                  height: double.infinity,
                  child: Image.asset(Assets.street, fit: BoxFit.fitHeight,)),
              BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 1, sigmaY: 1),
                child: Container(
                  padding: EdgeInsets.only(top: 60),
                  width: double.infinity,
                  height: double.infinity,
                  color: Colors.black54,
                  child: CircularList(
                    size: size.width,
                    itemSize: Size(80, 80),
                    duration: Duration(seconds: 1),
                    initialCount: 4,
                    padding: 30,
                    builder: (BuildContext context, int index) {
                      return Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage(
                                  Assets.player[index]
                              ),
                              fit: BoxFit.fill,
                              colorFilter: ColorFilter.mode(
                                Colors.black, BlendMode.hue,)
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(

                        image: AssetImage(
                            Assets.smoke
                        ), fit: BoxFit.fill
                    ),
                  ),
                ),
              ),

              Align(
                alignment: Alignment.bottomCenter,
                child: Container(height: cardHeight * 1.5,
                  width: double.infinity,
                  decoration: BoxDecoration(

                    image: DecorationImage(image: AssetImage(Assets.wood),
                        fit: BoxFit.fill,
                        colorFilter: ColorFilter.mode(
                          Colors.black54, BlendMode.hue,)),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),),
                  ),
                ),
              )
            ],
          ),
        ),
      ),);
  }

  static const cardHeight = 100.0;
  static const widthScale = 0.7;
}
