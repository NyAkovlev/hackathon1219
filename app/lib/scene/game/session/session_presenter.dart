import 'dart:async';

import 'package:domain/api/network/event/room_event.dart';
import 'package:domain/api/network/event/session_event.dart';
import 'package:domain/api/session/session_res.dart';
import 'package:domain/model/network/room/room_data.dart';
import 'session_view.dart';
import 'package:mpv/mpv.dart';
import 'package:domain/api/network/network_api.dart';
import 'package:domain/model/network/session/session.dart';
import 'package:domain/model/network/event/event_data.dart';
import 'package:domain/model/network/card/card_data.dart';


class SessionPresenter extends Presenter<SessionView> {
  final SessionRes res;
  final SessionEvent _sessionEvent;
  final RoomEvent _roomEvent;
  final NetworkApi _networkApi;
  final List<StreamSubscription> roomSubscription = [];
  final List<StreamSubscription> sessionSubscription = [];


  SessionPresenter(SessionView view, this.res, this._sessionEvent,
      this._roomEvent, this._networkApi) : super(view) {
    sessionListener();
  }

  toRoom(int id) {
    roomListener(id);
  }

  roomListener(int location) async {
    roomClear();

    roomSubscription.add(
        _roomEvent.onRoomData(location).listen((RoomData roomData) {

        }));
  }

  sessionListener() async {
    sessionClear();

    sessionSubscription.add(
        _sessionEvent.onStatus().listen((SessionStatus status) {
          res.onStatus(status);
        }));

    sessionSubscription.add(
        _sessionEvent.onEvent().listen((EventData event) {
          res.onEvent(event);
        }));

    sessionSubscription.add(
        _sessionEvent.removeEvent().listen((EventData event) {
          res.removeEvent(event);
        }));

    view.cardAdd = _sessionEvent.addCard().map((CardData card) {
      res.addCard(card);
      return card;
    });

    view.cardRemove = _sessionEvent.deleteCard().map((CardData card) {
      res.deleteCard(card);
      return card;
    });
  }

  pause() async {
    await _sessionEvent.pause();
  }

  resume() async {
    await _sessionEvent.resume();
  }


  sessionClear() {
    sessionSubscription.forEach((item) => item.cancel());
    sessionSubscription.clear();
  }

  roomClear() {
    roomSubscription.forEach((item) => item.cancel());
    roomSubscription.clear();
  }

  close() {
    super.close();
    roomClear();
    sessionClear();
  }
}
