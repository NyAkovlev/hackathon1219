import 'package:app/assets/assets.dart';
import 'package:app/view/card/positioned_card.dart';
import 'package:domain/model/network/card/card_data.dart';
import 'package:flutter/cupertino.dart';

class GameCard extends PositionedCard {
  final GlobalKey<PositionedCardState> key = GlobalKey<PositionedCardState>();
  final CardData card;

  GameCard(
      this.card, Function onTap, Function(PositionedCardState, Offset) onTapUp)
      : super(
          PlayCard(AssetImage(Assets.frontCard)),
          PlayCardBack(AssetImage(Assets.backCard)),
          width: 70,
          height: 130,
          onTapUp: onTapUp,
        );
}
