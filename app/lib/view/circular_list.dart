import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CircularList extends StatefulWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size(size, size);
  final double size;
  final double padding;
  final Size itemSize;
  final Duration duration;
  final IndexedWidgetBuilder builder;
  final int initialCount;

  const CircularList({
    @required this.size,
    @required this.itemSize,
    @required this.duration,
    @required this.builder,
    Key key,
    this.padding = 0,
    this.initialCount = 0,
  }) : super(key: key);

  @override
  CircularListState createState() => CircularListState();
}

class CircularListState extends State<CircularList> {
  final _eventQueue = StreamController<_Event>();
  List<Widget> _widgets;
  List<Widget> _positionWidgets;

  @override
  void initState() {
    _widgets = List.generate(widget.initialCount, (i) {
      return widget.builder(context, i);
    });
    _doOnEvent();
    _positions();
    super.initState();
  }

  _doOnEvent() {
    _eventQueue.stream.asyncMap((event) {
      if (event.isInsert) {
        return _insert();
      } else {
        return _remove(event.index);
      }
    }).listen((_) {});
  }

  @override
  void dispose() {
    _eventQueue.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.fromSize(
      size: widget.preferredSize,
      child: Stack(
        children: _positionWidgets,
      ),
    );
  }

  _positions({int insertIndex = -1, int removeIndex}) {
    final count = removeIndex == null ? _widgets.length : _widgets.length - 1;
    final size = widget.size - widget.padding * 2;
    final side = pi * 2 / count;
    final start = -pi / 2;
    _positionWidgets = List.generate(_widgets.length, (i) {
      var _i = i;
      if (removeIndex != null && i > removeIndex) {
        _i = i - 1;
      }
      final isRemove = i == removeIndex;
      final isInsert = insertIndex == i;
      return _position(
        (size / 2),
        (isInsert | isRemove) ? size : (size / 2),
        start + side * (isRemove ? i : _i),
        widget.itemSize,
        widget.padding,
        _widgets[i],
      );
    });
  }

  Widget _position(
    double center,
    double radius,
    double angle,
    Size itemSize,
    double padding,
    Widget child,
  ) {
    final size = sqrt(itemSize.width * itemSize.width +
            itemSize.height * itemSize.height) /
        2;
    final _radius = radius - size;
    return AnimatedPositioned(
      key: ValueKey(child.hashCode),
      duration: widget.duration,
      left: padding + (center - itemSize.width / 2 + _radius * cos(angle)),
      top: padding + (center - itemSize.height / 2 - _radius * sin(angle)),
      width: itemSize.width,
      height: itemSize.height,
      child: child,
    );
  }

  insert() {
    _eventQueue.add(_Event(true, null));
  }

  remove(Key index) {
    _eventQueue.add(_Event(false, index));
  }

  _insert() async {
    _widgets.insert(_widgets.length, widget.builder(context, _widgets.length));
    _positions(insertIndex: _widgets.length - 1);
    setState(() {});
    await Future.delayed(Duration(milliseconds: 100));
    _positions();
    setState(() {});
    await Future.delayed(widget.duration);
  }

  _remove(Key key) async {
    final index = _widgets.indexWhere((item) => item.key == key);
    _positions(removeIndex: index);
    setState(() {});
    await Future.delayed(widget.duration);
    _widgets.remove(_widgets[index]);
  }
}

class _Event {
  final bool isInsert;
  final Key index;

  _Event(this.isInsert, this.index);
}
