import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

abstract class PositionedCard extends StatefulWidget {
  final Widget front;
  final Widget back;
  final Function(PositionedCardState, Offset) onTapDown;
  final Function(PositionedCardState, Offset) onTapUp;
  final double left;
  final double top;
  final double width;
  final double height;

  const PositionedCard(
    this.front,
    this.back, {
    Key key,
        this.onTapDown, this.onTapUp,
        this.left,
        this.top,
        this.width,
        this.height,}) : super(key: key);

  @override
  PositionedCardState createState() => PositionedCardState();
}

class PositionedCardState extends State<PositionedCard>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  static const dragDuration = Duration(milliseconds: 300);
  Size size;

  Offset previous;
  double left;
  double top;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: dragDuration);
    left = widget.left ?? 0;
    top = widget.top ?? 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      width: widget.width,
      height: widget.height,
      curve: Curves.decelerate,
      duration: dragDuration,
      left: left,
      top: top,
      child: GestureDetector(
        onScaleStart: (state) {
          if (widget.onTapDown != null) widget.onTapDown(
              this, state.localFocalPoint);
          previous = state.localFocalPoint;
        },
        onScaleEnd: (state) {
          if (widget.onTapUp != null) widget.onTapUp(this, previous);
        },
        onScaleUpdate: (state) {
          final diff = previous - state.localFocalPoint;
          top -= diff.dy;
          left -= diff.dx;
          previous = state.localFocalPoint;
          setState(() {});
        },
        child: AnimatedBuilder(
          animation: _animationController,
          builder: (context, child) {
            final value = _animationController.value;
            if (value == 0) {
              return child;
            }
            var _child = value >= 0.5 ? widget.back : child;
            final radians = (_end - _start) * value;
            return SizedBox.fromSize(
              size: size,
              child: Transform(
                origin: Offset(size.width / 2, size.height / 2),
                transform: Matrix4.rotationY(_start + radians),
                child: _child,
              ),
            );
          },
          child: widget.front,
        ),
      ),
    );
  }

  to(double left, double top, [double rotate=0]) {
    if (this.left == left && this.top == top ) {
      return false;
    }
    this.left = left;
    this.top = top;
    setState(() {});
    return true;
  }

  flip() {
    size = context.size;
    if (_animationController.value == 0) {
      _animationController.animateTo(1, duration: Duration(seconds: 1));
    } else {
      _animationController.animateTo(0, duration: Duration(seconds: 1));
    }
  }

  static const _start = 0;

  static const _end = _start + pi;
}

class PlayCard extends StatelessWidget {
  final ImageProvider image;


  const PlayCard(this.image);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(image: image,
          fit: BoxFit.fill,
          colorFilter: ColorFilter.mode(
            Colors.brown.withAlpha(120), BlendMode.darken,),),
        borderRadius: BorderRadius.all(Radius.circular(10)),
        border: Border.all(color: Colors.black, width: 1),
        color: Colors.white,
        boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 4)],
      ),
    );
  }
}

class PlayCardBack extends StatelessWidget {
  final ImageProvider image;

  const PlayCardBack(this.image);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(image: image, fit: BoxFit.fill),
        borderRadius: BorderRadius.all(Radius.circular(10)),
        border: Border.all(color: Colors.black, width: 1),
        color: Colors.grey,
        boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 4)],
      ),
    );
  }
}
