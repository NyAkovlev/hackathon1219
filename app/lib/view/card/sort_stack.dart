import 'dart:math';

import 'positioned_card.dart';
import 'package:flutter/widgets.dart';

class SortStack extends StatefulWidget implements PreferredSizeWidget {
  final List<PositionedCard> cards;
  final Widget background;
  final Size size;
  final Size cardSize;
  final Offset packPoint;

  const SortStack({
    Key key,
    this.cards,
    this.size,
    this.background,
    this.cardSize,
    this.packPoint = Offset.zero,
  }) : super(key: key);

  @override
  SortStackState createState() => SortStackState();

  @override
  Size get preferredSize => size;
}

class SortStackState extends State<SortStack> {
  @override
  Widget build(BuildContext context) {
    return SizedBox.fromSize(
      size: widget.size,
      child: Stack(
        children: [
          widget.background,
          ...widget.cards,
        ],
      ),
    );
  }

  pack([Duration delayForEach]) async {
    for (PositionedCard item in widget.cards) {
      var key = item.key as GlobalKey<PositionedCardState>;
      final edited = key.currentState.to(
          widget.packPoint.dy, widget.packPoint.dx);
      if (edited && delayForEach != null) {
        await Future.delayed(delayForEach);
      }
    }
  }

  sort<T extends PositionedCard>(
      {Duration delayForEach, bool Function(T) test}) async {
    if (widget.cards.isEmpty) {
      return;
    }

    final padding = 10;
    final columns = (widget.cards.length).round() + 1;
    final sizeWidth = (widget.size.width - padding * 2);

    var i = -1;

    for (PositionedCard item in widget.cards) {
      i++;
      if (test == null || test(item)) {
        final left = sizeWidth * (i / columns);

        final key = item.key as GlobalKey<PositionedCardState>;

        final edited = key.currentState.to(
            widget.packPoint.dy + left + padding,
            widget.packPoint.dx + padding);
        if (edited && delayForEach != null) {
          await Future.delayed(delayForEach);
        }
      }
    }
  }

  inTop(PositionedCard card) {
    widget.cards.remove(card);
    widget.cards.add(card);
    setState(() {});
  }

  Future update() async {
    setState(() {});
    await Future.delayed(Duration(milliseconds: 100));
  }
}
