class Assets {
  static const _assets = "lib/res/";

  static const frontCard = _assets + "front_card.jpg";
  static const backCard = _assets + "back_card.jpg";
  static const wood = _assets + "wood.jpeg";
  static const smoke = _assets + "smoke.jpg";
  static const work = _assets + "work.jpg";
  static const home = _assets + "home.jpg";
  static const street = _assets + "street.jpg";

  static const player = [
    _assets + "player0.jpg",
    _assets + "player1.jpg",
    _assets + "player2.jpg",
    _assets + "player3.jpg"
  ];
}
