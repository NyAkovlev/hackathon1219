import 'package:app/scene/game/await_session/await_session_widget.dart';
import 'package:app/scene/game/session/session_widget.dart';
import 'package:domain/api/session/session_res.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppNavigator {
  GlobalKey<NavigatorState> _navigatorKey;

  NavigatorState get _navigator => _navigatorKey.currentState;

  setNavigator(GlobalKey<NavigatorState> key) {
    _navigatorKey = key;
  }

  toMain() {

  }

  toAwaitSession() {
    _navigator.pushReplacement(_route(AwaitSessionWidget()));
  }

  Route _route(Widget widget) {
    return MaterialPageRoute(builder: (BuildContext context) => widget);
  }

  startSession(SessionRes res) {
    _navigator.pushReplacement(_route(SessionWidget(res)));
  }
}
