import 'package:app/di.dart';
import 'package:app/navigator/app_navigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:app/scene/main/main_widget.dart';
class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AppState();
}

class AppState extends State<App> {
  final navigatorKey = GlobalKey<NavigatorState>();
  bool isLoad = true;

  AppNavigator _navigator;


  @override
  void initState() {
    super.initState();
    DI.init().then((_) {
      isLoad = false;
      _navigator = DI.get();
      _navigator.setNavigator(navigatorKey);
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isLoad) {
      return Container();
    } else {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        navigatorKey: navigatorKey,
        home: MainWidget(),
      );
    }
  }
}
