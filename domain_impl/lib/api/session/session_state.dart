import 'package:domain/model/network/card/card_type.dart';
import 'package:domain/model/network/event/event_type.dart';
import 'package:domain/model/network/room/room.dart';
import 'package:domain/model/network/room/room_type.dart';
import 'package:domain/model/network/user/user.dart';
import 'package:domain/model/network/event/event.dart';
import 'package:domain/model/network/card/card.dart';
import 'package:domain/model/network/session/session.dart';

abstract class SessionResImpl {
  final List<User> user = [];

  final List<Event> event = [];

  final List<EventType> eventType = [];

  final List<Card> card = [];

  final List<CardType> cardType = [];

  final List<Room> room = [];

  final List<RoomType> roomType = [];

  Future load(Session session);
}
