import 'package:domain/api/storage/delegate.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class Preference<T> extends Delegate<T> {
  final SharedPreferences _preferences;
  final String key;

  Preference(this._preferences, this.key);

  @override
  T get() {
    return _preferences.get(key);
  }
}

class PreferenceString extends Preference<String> {
  PreferenceString(SharedPreferences preferences, String key)
      : super(preferences, key);

  @override
  Future set(String value) {
    return _preferences.setString(key, value);
  }
}
