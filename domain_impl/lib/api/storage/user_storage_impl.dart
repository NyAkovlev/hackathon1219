import 'package:domain/api/storage/delegate.dart';
import 'package:domain/api/storage/user_storage.dart';
import 'package:domain_impl/api/storage/preference.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserStorageImpl extends UserStorage {
  @override
  final Delegate<String> userId;
  @override
  final Delegate<String> sessionId;

  UserStorageImpl(SharedPreferences preferences)
      : userId = PreferenceString(preferences, "id"),
        sessionId = PreferenceString(preferences, "sessionId");
}
