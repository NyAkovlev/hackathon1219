import 'package:domain/api/network/network_api.dart';
import 'package:domain/model/network/card/card.dart';
import 'package:domain/model/network/card/card_type.dart';
import 'package:domain/model/network/event/event.dart';
import 'package:domain/model/network/event/event_type.dart';
import 'package:domain/model/network/room/room.dart';
import 'package:domain/model/network/room/room_type.dart';
import 'package:domain/model/network/user/user.dart';
import 'package:domain/model/network/session/session.dart';
import 'package:domain_impl/model/map_util.dart';
import 'package:firebase_database/firebase_database.dart' as fb;

class NetworkApiImpl extends NetworkApi {
  final fb.FirebaseDatabase _firebaseDatabase;

  NetworkApiImpl(this._firebaseDatabase);

  @override
  Future<Session> getSession(String id) {
    return _firebaseDatabase
        .reference()
        .child("session")
        .child(id)
        .once()
        .then((map) {
      return Session.fromJson(MapUtil.toMap(map.value));
    });
  }

  @override
  Future<List<CardType>> getCardTypes() {
    return _firebaseDatabase.reference().child("cardType").once().then((item) {
      final iterable =
          item.value.map((item) => CardType.fromJson(MapUtil.toMap(item)));

      return List.of(iterable).cast<CardType>();
    });
  }

  @override
  Future<List<Card>> getCards() {
    return _firebaseDatabase.reference().child("card").once().then((item) {
      final iterable =
          item.value.map((item) => Card.fromJson(MapUtil.toMap(item)));

      return List.of(iterable).cast<Card>();
    });
  }

  @override
  Future<List<EventType>> getEventTypes() {
    return _firebaseDatabase.reference().child("eventType").once().then((item) {
      final iterable =
          item.value.map((item) => EventType.fromJson(MapUtil.toMap(item)));

      return List.of(iterable).cast<EventType>();
    });
  }

  @override
  Future<List<Event>> getEvents() {
    return _firebaseDatabase.reference().child("event").once().then((item) {
      final iterable =
          item.value.map((item) => Event.fromJson(MapUtil.toMap(item)));

      return List.of(iterable).cast<Event>();
    });
  }

  @override
  Future<List<RoomType>> getRoomTypes() {
    return _firebaseDatabase.reference().child("roomType").once().then((item) {
      final iterable =
          item.value.map((item) => RoomType.fromJson(MapUtil.toMap(item)));

      return List.of(iterable).cast<RoomType>();
    });
  }

  @override
  Future<List<Room>> getRooms() {
    return _firebaseDatabase.reference().child("room").once().then((item) {
      final iterable =
          item.value.map((item) => Room.fromJson(MapUtil.toMap(item)));

      return List.of(iterable).cast<Room>();
    });
  }

  @override
  Future<List<User>> getUsers() {
    return _firebaseDatabase.reference().child("user").once().then((item) {
      final iterable =
          item.value.map((item) => User.fromJson(MapUtil.toMap(item)));

      return List.of(iterable).cast<User>();
    });
  }

}
