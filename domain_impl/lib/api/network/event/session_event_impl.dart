import 'package:domain/api/network/event/session_event.dart';
import 'package:domain/api/storage/user_storage.dart';
import 'package:domain/model/network/card/card_data.dart';
import 'package:domain/model/network/event/event_data.dart';
import 'package:domain/model/network/session/session.dart';
import 'package:domain_impl/model/map_util.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

class SessionEventImpl extends SessionEvent {
  final FirebaseDatabase _firebaseDatabase;
  final UserStorage _userStorage;

  SessionEventImpl(this._firebaseDatabase, this._userStorage);


  @override
  Future<Session> awaitReady() async {
    await _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("user")
        .child(_userStorage.userId.get())
        .child("status")
        .set(describeEnum(SessionStatus.ready));

    await _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("status")
        .onValue
        .firstWhere((item) {
      return item?.snapshot?.value == describeEnum(SessionStatus.ready);
    });

    return _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .once()
        .then((item) => Session.fromJson(MapUtil.toMap(item.value)));
  }

  @override
  Future completeLoad() {
    return _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("user")
        .child(_userStorage.userId.get())
        .child("status")
        .set(describeEnum(SessionStatus.loaded));
  }

  @override
  Stream<EventData> onEvent() {
    final ref = _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("event");
    return MergeStream([
      ref.onChildChanged,
      ref.onChildAdded,
    ]).map((item) {
      return EventData.fromJson(MapUtil.toMap(item.snapshot.value));
    });
  }

  @override
  Stream<EventData> removeEvent() {
    final ref = _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("event");
    return MergeStream([
      ref.onChildRemoved,
    ]).map((item) {
      return EventData.fromJson(MapUtil.toMap(item.snapshot.value));
    });
  }


  @override
  Stream<SessionStatus> onStatus() {
    return _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("status")
        .onValue
        .map((item) {
      return SessionStatusMap.fromString(item?.snapshot?.value);
    });
  }

  @override
  Future pause() {
    return _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("user")
        .child(_userStorage.userId.get())
        .child("status")
        .set(describeEnum(SessionStatus.pause));
  }

  @override
  Future resume() {
    return _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("user")
        .child(_userStorage.userId.get())
        .child("status")
        .set(describeEnum(SessionStatus.run));
  }

  @override
  Stream<CardData> addCard() {
    final ref = _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("user")
        .child(_userStorage.userId.get())
        .child("card");

    return ref.onChildAdded.map((item) {
      return CardData.fromJson(MapUtil.toMap(item.snapshot.value));
    });
  }

  @override
  Stream<CardData> deleteCard() {
    final ref = _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("user")
        .child(_userStorage.userId.get())
        .child("card");

    return ref.onChildRemoved.map((item) {
      return CardData.fromJson(MapUtil.toMap(item.snapshot.value));
    });
  }
}
