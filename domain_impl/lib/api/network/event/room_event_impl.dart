import 'package:domain/api/network/event/room_event.dart';
import 'package:domain/api/storage/user_storage.dart';
import 'package:domain/model/network/room/room_data.dart';
import 'package:domain_impl/model/map_util.dart';
import 'package:firebase_database/firebase_database.dart' as fb;
import 'package:rxdart/rxdart.dart';

class RoomEventImpl extends RoomEvent {
  final fb.FirebaseDatabase _firebaseDatabase;
  final UserStorage _userStorage;

  RoomEventImpl(this._firebaseDatabase, this._userStorage);

  @override
  Stream<RoomData> onRoomData(int location) {
    final ref = _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("room")
        .child(location.toString());

    return MergeStream([
      ref.onChildAdded,
      ref.onChildChanged,
    ]).map((item) {
      return RoomData.fromJson(MapUtil.toMap(item.snapshot.value));
    });

  }


  @override
  Stream<RoomData> onRemoveData(int location) {
    final ref = _firebaseDatabase
        .reference()
        .child("session")
        .child(_userStorage.sessionId.get())
        .child("room")
        .child(location.toString());

    return MergeStream([
      ref.onChildRemoved,
    ]).map((item) {
      return RoomData.fromJson(MapUtil.toMap(item.snapshot.value));
    });
  }


}
