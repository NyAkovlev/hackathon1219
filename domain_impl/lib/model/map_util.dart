class MapUtil {
  static Map<String, dynamic> toMap(Map map) {
    Map<String, dynamic> list = {};
    map?.forEach((key, value) {
      list[key] = _convert(value);
    });
    return list;
  }

  static _convert(value) {
    if (value is List) {
      return value?.map((item) => _convert(item))?.toList();
    } else if (value is Map) {
      return toMap(value);
    } else {
      return value;
    }
  }
}
